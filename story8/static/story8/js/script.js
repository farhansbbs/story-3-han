$(document).ready(function () {    
    $("#keyword").keyup( function(){
        var ketikan = $("#keyword").val();
        console.log(ketikan);

        $.ajax({
            url: "/story8/data?q=" + ketikan,
            success: function(data) {
                // console.log(data);
                var header = "<thead class='thead-dark'><tr><th>Judul</th><th>Penulis</th><th>Penerbit</th><th>Sampul Buku</th></tr></thead>"
                var array_items = data.items;
                console.log(array_items);
                $("#list-buku").empty();
                $("#list-buku").append(header);

                for(i = 0; i < array_items.length; i++) {
                    var title = array_items[i].volumeInfo.title;
                    var author = array_items[i].volumeInfo.authors;
					var publisher = array_items[i].volumeInfo.publisher;
                    var cover = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var link = array_items[i].volumeInfo.infoLink;
                    // console.log(link);
                    $("#list-buku").append("<tr><td><a href=" + link + " target='_blank'>" + title + "</a></td><td>" + author + "</td><td>" + publisher + "</td><td><img src=" + cover + "></td></tr>");
                    // console.log(judul);
                }
            }
        });
    }); 
});