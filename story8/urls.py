from django.urls import path

from .views import index, search

app_name = 'story8'

urlpatterns = [
    path('', index, name='story8_index'),
    path('data/', search, name='story8_data'),
]