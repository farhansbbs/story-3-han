from django.test import TestCase, Client
from django.urls import resolve

from .views import index

# Create your tests here.
class Story8Test(TestCase):
    
    # URL Test
    def test_url_is_exist_story8(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    # View Test
    def test_view_story8_is_using_function_index(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    # Template Test
    def test_template_story8_is_using_index_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')