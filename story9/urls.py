from django.urls import path

from .views import index, user_login, user_logout, user_signup

app_name = 'story9'

urlpatterns = [
    path('', index, name='index'),
    path('signup/', user_signup, name='signup'),
    path('login/', user_login, name='login'),
    path('logout/', user_logout, name='logout'),
]