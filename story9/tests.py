from django.test import TestCase, Client
from django.urls import resolve

from .views import index

# Create your tests here.
class Story9Test(TestCase):
    
    # URL Test
    def test_url_is_exist_story9(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    # View Test
    def test_view_story9_is_using_function_index(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)

    # Template Test
    def test_template_story9_is_using_index_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')