from django.shortcuts import render, redirect, resolve_url
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def index(request):
    return render(request, 'story9/index.html')

def user_signup(request):
    if (not request.user.is_anonymous):
        return redirect(resolve_url('story9:index'))

    context = {
        'error': False
    }

    if (request.method == "POST"):
        user = None
        try:
            user = User.objects.get(username = request.POST['username'])
        except:
            user == None

        if(user == None):
            new_user = User.objects.create_user(request.POST['username'], '', request.POST['password'])
            new_user.save()
            login(request, new_user)
            return redirect(resolve_url('story9:index'))
        else:
            context['error'] = True
            return render(request, 'story9/signup.html', context)

    return render(request, 'story9/signup.html', context)

def user_login(request):
    if (request.user.is_authenticated):
        return redirect(resolve_url('story9:index'))

    context = {
        'error': False
    }

    if (request.method == "POST"):
        user = authenticate(
            username = request.POST['username'],
            password = request.POST['password'],
        )

        if (user == None):
            context['error'] = True
            return render(request, 'story9/login.html', context)
        else:
            login(request, user)
            return redirect(resolve_url('story9:index'))

    return render(request, 'story9/login.html', context)

def user_logout(request):
    logout(request)
    return redirect(resolve_url('story9:index'))