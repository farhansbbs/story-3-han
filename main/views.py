from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def story1(request):
    return render(request, 'main/story1.html')

def story3(request):
    return render(request, 'main/story3.html')

def bonus(request):
    return render(request, 'main/bonus.html')