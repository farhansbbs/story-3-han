from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    kode = models.CharField(max_length=10)
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.PositiveIntegerField(default=0)
    deskripsi = models.CharField(max_length=1000)

    def __str__(self):
        return self.nama