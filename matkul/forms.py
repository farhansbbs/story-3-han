from django import forms

from .models import MataKuliah

class MataKuliahCreateForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = '__all__'
        widgets = {
            'kode' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'CSGE601020',
            }),
            'nama' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Dasar-dasar Pemrograman 1',
            }),
            'dosen' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Drs. Lim Yohanes Stefanus M.Math., Ph.D.',
            }),
            'sks' : forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'deskripsi' : forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Mata Kuliah Wajib Fakultas',
            }),
        }