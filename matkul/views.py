from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import MataKuliahCreateForm


def matkulView(request):
    allMatkul = MataKuliah.objects.all()
    context = {'allMatkul': allMatkul}
    return render(request, 'matkul/matkul.html', context)

def matkulCreate(request):
    form = MataKuliahCreateForm(request.POST or None)
    if(form.is_valid() and request.method == 'POST'):
        form.save()
    
    context = {'form': form}
    return render(request, 'matkul/matkul_create.html', context)

def details(request, key):
    matkul = MataKuliah.objects.get(id=key)
    context = {'matkul': matkul}
    return render(request, 'matkul/matkul_details.html', context)

def delete(request, key):
    matkul = MataKuliah.objects.get(id=key)
    matkul.delete()
    return redirect('/matkul')