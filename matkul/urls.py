from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.matkulView, name='matkul'),
    path('create', views.matkulCreate, name='create'),
    path('delete/<str:key>', views.delete, name='delete'),
    path('details/<str:key>', views.details, name='details'),
]