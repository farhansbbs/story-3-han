from django import forms

from .models import Aktivitas, Peserta

class AktifitasForm(forms.ModelForm):
    class Meta:
        model = Aktivitas
        fields = '__all__'
        widgets = {
            'nama' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder' : 'Masukkan nama kegiatan'
            })
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'
        widgets = {
            'nama' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder' : 'Tambahkan peserta'
            })
        }