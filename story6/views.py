from django.shortcuts import render
from .models import Aktivitas, Peserta
from .forms import AktifitasForm, PesertaForm

# Create your views here.
def index(request):
    return render(request, 'story6/index.html')

def tambah_aktivitas(request):
    pass

def hapus_aktivitas(request):
    pass

def tambah_peserta(request):
    pass

def hapus_peserta(request):
    pass