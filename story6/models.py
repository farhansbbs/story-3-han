from django.db import models

# Create your models here.
class Aktivitas(models.Model):
    nama = models.CharField(max_length=50, default="")

class Peserta(models.Model):
    nama = models.CharField(max_length=50, default="")
    aktivitas = models.ForeignKey(Aktivitas, on_delete=models.CASCADE)