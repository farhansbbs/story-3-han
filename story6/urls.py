from django.urls import path

from .views import index

app_name = 'story6'

urlpatterns = [
    path('', index, name='story6_index'),
]