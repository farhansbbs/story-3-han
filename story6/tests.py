from django.test import TestCase, Client
from django.urls import resolve

from .views import index
from .models import Aktivitas, Peserta

# Create your tests here.
class Story6Test(TestCase):
    # URL Test
    def test_url_is_exist_story6(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    # View Test
    def test_view_story6_is_using_function_index(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    # Template Test
    def test_template_story6_is_using_index_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html')

    # Model Test
    def test_model_aktivitas_create(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'aktivitas baru')

        count_aktivitas = Aktivitas.objects.all().count()
        self.assertEqual(count_aktivitas, 1)
    
    def test_model_peserta_create(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'aktivitas baru')
        peserta_baru = Peserta.objects.create(nama = 'peserta baru', aktivitas = aktivitas_baru)

        count_peserta = Peserta.objects.all().count()
        self.assertEqual(count_peserta, 1)

    def test_model_aktivitas_delete(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'aktivitas baru')
        aktivitas_baru.delete()

        count_aktivitas = Aktivitas.objects.all().count()
        self.assertEqual(count_aktivitas, 0)

    def test_model_peserta_delete(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'aktivitas baru')
        peserta_baru = Peserta.objects.create(nama = 'peserta baru', aktivitas = aktivitas_baru)
        peserta_baru.delete()

        count_peserta = Peserta.objects.all().count()
        self.assertEqual(count_peserta, 0)
    
    def test_model_peserta_cascade(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'aktivitas baru')
        peserta_baru = Peserta.objects.create(nama = 'peserta baru', aktivitas = aktivitas_baru)
        aktivitas_baru.delete()

        count_aktivitas = Aktivitas.objects.all().count()
        count_peserta = Peserta.objects.all().count()
        self.assertEqual(count_aktivitas, 0)
        self.assertEqual(count_peserta, 0)