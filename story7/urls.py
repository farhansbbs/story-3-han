from django.urls import path

from .views import index

app_name = 'story7'

urlpatterns = [
    path('', index, name='story7_index'),
]